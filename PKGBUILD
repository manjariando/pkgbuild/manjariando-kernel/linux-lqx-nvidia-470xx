# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>

_linuxprefix=linux-lqx
_extramodules=extramodules-6.2-lqx
# don't edit here
pkgver=470.182.03_6.2.10.lqx1_1

_nver=470
# edit here for new version
_sver=182.03
# edit here for new build
pkgrel=1
pkgname=$_linuxprefix-nvidia-470xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=('x86_64')
url="http://www.nvidia.com/"
depends=("nvidia-470xx-utils=${_pkgver}")
makedepends=("$_linuxprefix" "$_linuxprefix-headers")
groups=("$_linuxprefix-extramodules")
provides=("$_pkgname=$_pkgver" "$_linuxprefix-nvidia-418xx" "$_linuxprefix-nvidia-430xx"
          "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-440xx" "$_linuxprefix-nvidia-450xx")
conflicts=("$_linuxprefix-nvidia-340xx" "$_linuxprefix-nvidia-390xx" "$_linuxprefix-nvidia-418xx"
           "$_linuxprefix-nvidia-430xx" "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-440xx"
           "$_linuxprefix-nvidia-450xx")
license=('custom')
install=nvidia.install
options=(!strip)
durl="https://us.download.nvidia.com/XFree86/Linux-x86"
source=("${durl}_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run")
sha256sums=('0a02d9341b9b4206df1401a812e8dfed2406bc1f3d7a1055260149cde858aa8c')

_pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    sh "${_pkg}.run" --extract-only

    # patches here
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${_pkg}"
    make -C kernel SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")

    cd "${_pkg}"
    install -Dm644 kernel/*.ko -t "${pkgdir}/usr/lib/modules/${_extramodules}/"

    # compress each module individually
    find "${pkgdir}" -name '*.ko' -exec xz -T1 {} +

    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
